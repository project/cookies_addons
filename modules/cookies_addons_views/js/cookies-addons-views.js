/**
 * @file
 * Defines Javascript behaviors for the Cookies Addons views module.
 */
((Drupal, $) => {
  /**
   * Define defaults.
   */
  Drupal.behaviors.cookiesAddonsViews = {
    activate(service, context) {
      $('.cookies-addons-views-placeholder', context).each(function () {
        if (!$(this).hasClass('request-sent')) {
          // Handle views added by the viewsreference module.
          const $container = $(this).parents('.views-element-container');
          if ($container.length) {
            $container.replaceWith($(this));
          }

          const cookiesService = $(this).attr('cookies-service');
          if (cookiesService === service) {
            $(this).addClass('request-sent');

            const viewId = $(this).attr('view-id');
            const displayId = $(this).attr('display-id');
            const args = $(this).data('args');

            Drupal.ajax({
              url: `/cookies-addons-views/get-view/${viewId}/${displayId}/${service}/${args}`,
            }).execute();
          }
        }
      });
    },

    fallback(service, context) {
      $(
        `.cookies-addons-views-placeholder[cookies-service='${service}']`,
        context,
      ).each(function () {
        const serviceName = $(this).attr('service-name');
        $(this).cookiesOverlay(service, serviceName);
      });
    },

    attach(context) {
      const self = this;

      document.addEventListener('cookiesjsrUserConsent', (event) => {
        const services =
          typeof event.detail.services === 'object'
            ? event.detail.services
            : {};
        $.each(services, (service, accepted) => {
          if (accepted) {
            self.activate(service, context);
          } else {
            self.fallback(service, context);
          }
        });
      });
    },
  };
})(Drupal, jQuery);
