/**
 * @file
 * Defines Javascript behaviors for the Cookies Addons blocks module.
 */
((Drupal, $) => {
  /**
   * Define defaults.
   */
  Drupal.behaviors.cookiesAddonsBlocks = {
    activate(service, context) {
      $('.cookies-addons-blocks-placeholder', context).each(function () {
        if (!$(this).hasClass('request-sent')) {
          // Handle blocks.
          const $container = $(this).parents('.block');
          if ($container.length) {
            $container.replaceWith($(this));
          }

          const cookiesService = $(this).data('cookies-service');
          if (cookiesService === service) {
            $(this).addClass('request-sent');

            const blockId = $(this).data('block-id');

            Drupal.ajax({
              url: `/cookies-addons-blocks/get-block/${blockId}/${service}`,
              type: 'POST',
            }).execute();
          }
        }
      });
    },

    fallback(service, context) {
      $(
        `.cookies-addons-blocks-placeholder[data-cookies-service='${service}']`,
        context,
      ).each(function () {
        const serviceName = $(this).data('service-name');
        $(this).cookiesOverlay(service, serviceName);
      });
    },

    attach(context) {
      const self = this;

      document.addEventListener('cookiesjsrUserConsent', (event) => {
        const services =
          typeof event.detail.services === 'object'
            ? event.detail.services
            : {};
        $.each(services, (service, accepted) => {
          if (accepted) {
            self.activate(service, context);
          } else {
            self.fallback(service, context);
          }
        });
      });
    },
  };
})(Drupal, jQuery);
