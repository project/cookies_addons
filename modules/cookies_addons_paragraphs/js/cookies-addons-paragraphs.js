/**
 * @file
 * Defines Javascript behaviors for the Cookies Addons paragraphs module.
 */
((Drupal, $) => {
  /**
   * Define defaults.
   */
  Drupal.behaviors.cookiesAddonsParagraphs = {
    activate(service, context) {
      $('.cookies-addons-paragraph-placeholder', context).each(function () {
        if (!$(this).hasClass('request-sent')) {
          // Handle paragraphs.
          const $container = $(this).parents('.paragraph');
          if ($container.length) {
            $container.replaceWith($(this));
          }

          const cookiesService = $(this).data('cookies-service');
          if (cookiesService === service) {
            $(this).addClass('request-sent');

            const paragraphId = $(this).data('paragraph-id');

            Drupal.ajax({
              url: `/cookies-addons-paragraphs/get-paragraph/${paragraphId}/${service}`,
              type: 'POST',
            }).execute();
          }
        }
      });
    },

    fallback(service, context) {
      $(
        `.cookies-addons-paragraph-placeholder[data-cookies-service='${service}']`,
        context,
      ).each(function () {
        const serviceName = $(this).data('service-name');
        $(this).cookiesOverlay(service, serviceName);
      });
    },

    attach(context) {
      const self = this;

      document.addEventListener('cookiesjsrUserConsent', (event) => {
        const services =
          typeof event.detail.services === 'object'
            ? event.detail.services
            : {};
        $.each(services, (service, accepted) => {
          if (accepted) {
            self.activate(service, context);
          } else {
            self.fallback(service, context);
          }
        });
      });
    },
  };
})(Drupal, jQuery);
