/**
 * @file
 * Defines Javascript behaviors for the cookies addons embed iframe module.
 */
((Drupal, $) => {
  /**
   * Define defaults.
   */
  Drupal.behaviors.cookiesAddonsEmbedIframe = {
    consentGiven(context) {
      $('iframe.cookies-addons-embed-iframe', context).each((i, element) => {
        const $element = $(element);
        if ($element.attr('src') !== $element.data('src')) {
          $element.attr('src', $element.data('src'));
        }
      });
    },

    consentDenied(context) {
      $(
        'iframe.cookies-addons-embed-iframe, div.iframe-embed-lazy',
        context,
      ).cookiesOverlay('iframe');
    },

    attach(context) {
      const self = this;
      document.addEventListener('cookiesjsrUserConsent', (event) => {
        const service =
          typeof event.detail.services === 'object'
            ? event.detail.services
            : {};
        if (typeof service.iframe !== 'undefined' && service.iframe) {
          self.consentGiven(context);
        } else {
          self.consentDenied(context);
        }
      });
    },
  };
})(Drupal, jQuery);
